// comparison query operators

	// $gt/$gte operator - greater than/greater than or equal
		db.users.find({ age: {$gt : 50}});
		db.users.find({ age: {$gte : 50}});

	//  $lt/$lte operator - less than/less than or equal
		db.users.find({ age: {$lt : 50}});
		db.users.find({ age: {$lte : 50}});

	//  $ne operator - not equal
		db.users.find({ age: {$ne : 82}});

	// $in operator - specific match criteria one field sing different values
		db.users.find({ lastName: {$in : ["Hawking", "Doe"] }});
		db.users.find({ courses: {$in : ["HTML", "React"] }});

// logical query operators

	// $or operator
		db.users.find({ $or: [ {firstName: "Neil"}, {age: "25"} ] });
		db.users.find({ $or: [ {firstName: "Neil"}, {age: {$gt: 30} } ] });

	// $and operator
		db.users.find({ $and: [ {age: {$ne: 82}}, {age: {$ne: 76} } ] });

// field projection

	// inclusion 
	// - include/add specific field only when retrieving documents.
	// - the value provided is 1 to denote that the field is being included.

		db.users.find(
			{
				firstName: "Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				contact: 1
			}

		);


	// exclusion
	// - exclude/remove specific field only when retrieving documents.
	// - the value provided is 0 to denote that the field is being excluded.
		
		db.users.find(
			{
				firstName: "Jane"
			},
			{
				contact: 0,
				department: 0
			}

		);

	// inclusion and exclusion combined
	// suppressing the ID field

		db.users.find(

			{
				firstName:"Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				contact: 1,
				_id: 0
			}

		);

		db.users.find(

			{
				firstName:"Jane"
			},
			{
				firstName: 1,
				lastName: 1,
				"contact.phone": 1,
				_id: 0
			}

		);

		db.users.find(

			{
				firstName:"Jane"
			},
			{

				"contact.phone": 0,
			}

		);


	// $slice operator - retrieve only 1 element that matched the search criteria
		db.users.find(

			{ 
				"namearr" : 

					{
						namea: "juan"
					}
			},

			{
				namearr: {$slice: 1},
			}
		);


// evaluation query operators

	// $regex operator - match a specific string pattern using a regular expression

		// case sensitive
		db.users.find({ firstName: {$regex: "N"} });

		// case insensitive
		db.users.find({ firstName: {$regex: "j", $options: '$i'} });

